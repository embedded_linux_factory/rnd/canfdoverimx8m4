# CAN over IMX8 M4



## Build a Technexion Image with Yocto

The Yocto Project is a Linux Foundation collaborative open source project whose goal is to produce tools and processes that enable the creation of Linux distributions for embedded and IoT software that are independent of the underlying architecture of the embedded hardware.

This section is to build an Image with the PICO-IMX8MQ board. If you use another board from TechNexion, please refer to [Technexion Developer Portal](https://developer.technexion.com/docs/building-an-image-with-yocto).

First, update the host package list :
```bash
sudo apt-get update
```
Install necessary packages :
```bash
sudo apt-get install gawk wget git git-core diffstat unzip texinfo gcc-multilib build-essential \
chrpath socat cpio python python3 python3-pip python3-pexpect \
python3-git python3-jinja2 libegl1-mesa pylint3 rsync bc bison \
xz-utils debianutils iputils-ping libsdl1.2-dev xterm \
language-pack-en coreutils texi2html file docbook-utils \
python-pysqlite2 help2man desktop-file-utils \
libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake \
groff curl lzop asciidoc u-boot-tools libreoffice-writer \
sshpass ssh-askpass zip xz-utils kpartx vim screen
```
Install the Google's Repo Tool :
```bash
mkdir ~/bin

curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo

chmod a+x ~/bin/repo
```
Download the BSP source :
```bash
PATH=${PATH}:~/bin

mkdir <your_project_name>

cd <your_project_name>
```

Initialize the repository based on kikstone stable version :
```bash
repo init -u https://github.com/TechNexion/tn-imx-yocto-manifest.git -b kirkstone_5.15.y-stable -m imx-5.15.71-2.2.2.xml
```
If you want to use another branch, please refer to [Technexion Git](https://github.com/TechNexion/meta-tn-imx-bsp) and choose your branch.

Fetch the source code :
```bash
repo sync -j8
```
Setup the build configuration (in our case, PICO-IMX8MQ board):
```bash
WIFI_FIRMWARE=y DISTRO=fsl-imx-xwayland MACHINE=pico-imx8mq source tn-setup-release.sh -b build-xwayland-pico-imx8mq
```
Build the Xwaylan image full :
```bash
bitbake imx-image-full
```

## Loading Software using 'uuu' to flash eMMC 

This section takes you through the process to load e.MMC with the Universal Update Utility ('uuu'). This is the name of NXP's MfgTool 3.0. This tool is able to be executed under Windows and Linux environment and can be used to load and configure the e.MMC of images over a USB-OTG port.

Install required packages for running 'uuu' :
```bash
sudo apt-get install libusb-1.0.0-dev libzip-dev libbz2-dev
```
Prebuilt binaries of uuu can be download directly from this gitlab repo in the [Develop branch](https://gitlab.com/embedded_linux_factory/rnd/canfdoverimx8m4.git) or from TechNexion's [Download Site](https://download.technexion.com/development_resources/development_tools/installer/imx-mfg-uuu-tool.zip).

```bash
#Install git 

sudo apt install git-all

#Clone a repository

cd <path/to/your/local/repository>

git clone <URL_of_the_repository>

```
Please follow the README within the 'uuu'repository to build the tool from source.

Flash the board with...
```bash
#.bz2 compressed images

cd imx-mfg-uuu-tool
sudo ./uuu/linux64/uuu -b emmc_img imx8mq/pico-imx8mq/pico-imx8mq-flash.bin <path-to-image-to-flash>/<image name>.wic.bz2/*

#decompressed images
cd imx-mfg-uuu-tool
sudo ./uuu/linux64/uuu -b emmc_img imx8mq/pico-imx8mq/pico-imx8mq-flash.bin <path-to-image-to-flash>/<image name>.wic
```

## Setup Technexion Environment

Freertos Technexion SDK download :
```bash
git clone https://github.com/TechNexion/freertos-tn.git
```
Install required packages :
```bash
sudo apt install make cmake
```
Download and export the toolchain :
```bash
wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2018q2/gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2

tar jxvf gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2

export ARMGCC_DIR=<path/to/your/toolchain/directory>/gcc-arm-none-eabi-7-2018-q2-update

export AARCH=arm64
```

## Setup minicom

Install and setup minicom
```bash
sudo apt install minicom

sudo minicom -s
```
To configure minicom, use the arrow keys on your keyboard and the Enter key. Normally, when you connect your board to your computer, 2 devices should appear: `ttyUSB0` (for A53 cortex) and `ttyUSB1` (for m4 cortex).

`Enter Serial port` setup by presseing the "Enter" key.

Press "A" on your keyboard and replace the current `Serial Device` with `/dev/ttyUSB0`

Check that the E section (`Bps/Par/Bits`) is set to `115200 8N1`, otherwise press "E" and set it to this value
Press "F" on your keyboard to switch the `Hardware Flow Control` to `No`


Press the "Enter" key once to go back to the `minicom` configuration menu.
Go down to `Save setup as dfl` and press "Enter" to save the current configuration as default.
Go down to `Exit from Minicom` and press "Enter".

Now you only need to do 

```bash
sudo minicom
```
to get the board's serial output on ttyUSB0 (a53) and communicate with the board.

To get the board the board's setrial output on ttyUSB1 (m4), make the same thing realized for ttyUSB0 but instead of saving by default, just `Exit`. You have to configure ttyUSB1 every time you open a new terminal.

## Connect to the board via SSH

First of, find the right IP address to connect to the board (use `ifconfig` or `ip address` in `minicom` or `screen`).
By default, the board uses a dynamic IP address, so you will have to setup a fix IP address if you want to make things easier.
In `minicom`, go to the board's `/etc/systemd/network/` folder and create a new file titled `20-wired.netwrok`.
In this file, write :
```bash
[Match]
Name=eth0

[Network]
Address=<your_IP_address>/24
```

Save the file and reboot the board.
You should now be able to do `ssh root@<your_IP_address>`.

## Build and load binaries files from M4

In this section we will build and load the hello world binary from the freertos Technexion SDK. 

Please refer to the section above to succeed this part.

Build Hello World application : 
```bash
cd <path/to/your/sdk>/freertos-tn/board/<your_board>/rtos_examples/freertos_hello/armgcc

./build_debug.sh
```
After this step, you should have a new folder named debug where there are 2 files : hello_world.bin and hello_world.elf.

In a first terminal (1), connect to your board via SSH :
```bash
ssh root@<board_IP>
```

In a second terminal (2), connect to your board with minicom (ttyUSB0): 
```bash
sudo minicom
```

In a third terminal (3), open minicom on ttyUSB1 (m4).

(1) Create a project folder on your board : 
```bash
mkdir <board_project_name>
```
This folder will be where are binaries files.

(2) Copy hello_world.bin on the board :
```bash
cd </path/to/your/sdk>/freertos-tn/board/<your_board>/rtos_examples/freertos_hello/armgcc/debug

scp hello_world.bin root@<board_IP>:/home/root/<board_project_name>
```
Check that the file has been copied to the board.

Reboot your board and enter in U-boot.

List the mmc :

```bash
mmc list
```
In our case eMMC is 0 and the ext4load is partition 2 on MMC device. 

Configure U-Boot to automatically load an image from the second partition of the first MMC device detected at system startup :
```bash
setenv load_cmd "ext4load mmc 0:2"
```
Locate the file to be loaded in the board folders. In our case :
```bash
setenv m4image "/home/root/<board_project_name/hello_world.bin"
```
Define the image size
```bash
setenv m4image_size 15000
```

Automatically load the "m4image" image at the memory address specified by the loadaddr variable, using the load command defined in the load_cmd variable : 
```bash
setenv loadm4image "${load_cmd} ${loadaddr} ${m4image}"
```

U-Boot command that configures and starts the M4 secondary processor and loads the hello world application :
```bash
setenv m4boot "${loadm4image}; cp.b ${loadaddr} 0x7e0000 ${m4image_size}; dcache flush; bootaux 0x7e0000"
```
Save U-boot environment :
```bash
saveenv
```
Start the firmware at boot level :
```bash
run m4boot
```
Normally you should see the "Hello World" message appear in the terminal (3).

## How create his own layer 

In your Yocto sources, create your layer-folder and the following under folders and files
```bash 
cd <your_porject_name>/sources

mkdir meta-<your_layer_name>

cd meta-<your_layer_name>

mkdir conf

mkdir recipes-kernel

cd recipes-kernel

mkdir linux

cd linux

touch linux-tn-imx_%25.bbappend

mkdir files
```
Then, you have to fetch it in `bblayers.conf.org` located ine the `conf` folder of your build forlder. Open it and add the following lines :
```bash
# setup mycustomlayer in bblayers.conf
BBLAYERS += " ${BSPDIR}/sources/meta-<your_layer_name "
```
To finish, you will have to call your patch in the following file `linux-tn-imx_%25.bbappend` :
```bash
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://<name_of_your_futur_patch> \
    "
```

## How create a patch

Install quilt
```bash
sudo apt install quilt
```
Go to the place where the kernel sources are located and create your patch :
```bash
cd <path/to/your/kernel/sources>

quilt new <your_patch_name>.patch
```
Link the patch with the file to be modified, in our case imx8mq-pico-pi.dts
```bash
quilt add <path/to/the/files/that/you/want/modified>
```
After entering the command above you can make the changes to the file you want to change. 

Then, enter the following command : 
```bash
quilt refresh
```
Now your can see in your patch located in the patch folder the changes made. Copy your patch and paste it in your layer in the following folder `meta-<your_layer_name>/recipes-kernel/linux/files` : 

```bash
cp patches/<your_patch> ~/<your_project_name>/sources/meta-<your_layer_name>/recipes-kernel/linux/files
```
Then, in the file `linux-tn-imx_%25.bbappend` located `meta-<your_layer_name>/recipes-kernel/linux`, you can call your patch as described at the end of the previous section.

## Define Remoteproc and RPMsg nodes in the device tree

Using the method described above, add the following lines to the `imx8mq-pico-pi.dts` file: 
```bash
.
.
.
/ {
	model = "TechNexion PICO-IMX8MQ and PI baseboard";
	compatible = "pico,imx8mq-pico", "fsl,imx8mq";

	chosen {
		stdout-path = &uart1;
	};

/* Beginning of section to be added */
	
    reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		m4_reserved: m4@0x80000000 {
			no-map;
			reg = <0 0x80000000 0 0x1000000>;
		};

		vdev0vring0: vdev0vring0@b8000000 {
			reg = <0 0xb8000000 0 0x8000>;
			no-map;
		};

		vdev0vring1: vdev0vring1@b8008000 {
			reg = <0 0xb8008000 0 0x8000>;
			no-map;
		};

		rsc_table: rsc_table@b80ff000 {
			reg = <0 0xb80ff000 0 0x1000>;
			no-map;
		};

		vdevbuffer: vdevbuffer@b8400000 {
			compatible = "shared-dma-pool";
			reg = <0 0xb8400000 0 0x100000>;
			no-map;
		};
	};

	imx8mq-cm4 {
		compatible = "fsl,imx8mq-cm4";
		rsc-da = <0xb8000000>;
		clocks = <&clk IMX8MQ_CLK_M4_DIV>;
		mbox-names = "tx", "rx", "rxdb";
		mboxes = <&mu 0 1
			  &mu 1 1
			  &mu 3 1>;
		memory-region = <&vdevbuffer>, <&vdev0vring0>, <&vdev0vring1>, <&rsc_table>;
		syscon = <&src>;
		fsl,startup-delay-ms = <500>;
	};

    /* End of section to be added */
    .
    .
    .
```

To avoid memory conflicts, comment out the following part in the `imx8mq-pico.dtsi` file:
```bash
.
.
.
/*
	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		rpmsg_reserved: rpmsg@0xb8000000 {
			no-map;
			reg = <0 0xb8000000 0 0x400000>;
		};
	};
*/
.
.
.
```
After that, don't forget to apply your patch in your layer and rebuild the image.  You can then fash th board with the new image modified. 

## Interprocessor communication (IPC) with TTY application 

To implement IPC via RPMsg on the Pico-pi-imx8mq, Technexion supplies an SDK (called ‘freertos-tn’) based on the FreeRTOS RTOS, enabling applications to be develop applications on the M4. This SDK also provides ready-made applications for Technexion evaluation kits. To set up the IPC, I used this SDK, which provides demonstration applications for setting up this IPC.

In the [Develop branch](https://gitlab.com/embedded_linux_factory/rnd/canfdoverimx8m4.git) you will find the SDK sources with the corresponding firmwares. The firmwares are adapted for NXP evaluation boards. I therefore made the modifications to the firmware located in `freertos-tn-SDK-modified/boards/evkmimx8mq/multicore_examples/rpmsg_lite_str_echo_rtos` so that the firmware is perfectly adapted to our Technexion evaluation board. You can then directly retrieve the compiled firmware named `rpmsg_lite_str_echo_rtos.bin` from `freertos-tn-SDK-modified/boards/evkmimx8mq/multicore_examples/rpmsg_lite_str_echo_rtos/armgcc/debug`. You can then refer to the `Build and load binaries files from M4` section above to load the firmware into the M4.

Finir le readme avec le chargement du module etc ...

























